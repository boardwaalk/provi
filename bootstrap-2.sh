#!/bin/sh
set -e -x

# you may need --
# sysctl net.ipv6.conf.<device>.disable_ipv6=1

pacman --noconfirm --needed -S gcc make

curl -L -4 https://aur.archlinux.org/packages/pa/package-query/package-query.tar.gz | tar xz
pushd package-query
makepkg --asroot --noconfirm -s
pacman --noconfirm -U package-query-*.pkg.tar.xz
popd
rm -r package-query

curl -L -4 http://aur.archlinux.org/packages/ya/yaourt/yaourt.tar.gz | tar xz
pushd yaourt
makepkg --asroot --noconfirm -s
pacman --noconfirm -U yaourt-*.pkg.tar.xz
popd
rm -r yaourt

yaourt --noconfirm -S salt-git

sed -i 's/^#default_include:/default_include:/' /etc/salt/minion
mkdir /etc/salt/minion.d
echo "master: gammaburst.ersatsz.com" > /etc/salt/minion.d/00-master.conf
