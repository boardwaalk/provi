desktop-packages:
  pkg.installed:
    - pkgs:
      - ttf-dejavu
      - gtk-engine-murrine
      - firefox
      - flashplugin
      - vlc
      - alsa-plugins
      - rxvt-unicode
      - redshift
      - python2-xdg
      # group xfce4
      - exo
      - garcon
      #- gtk2-xfce-engine
      #- gtk3-xfce-engine
      - thunar
      - thunar-volman
      - tumbler
      - xfce4-appfinder
      - xfce4-mixer
      - xfce4-panel
      - xfce4-power-manager
      - xfce4-session
      - xfce4-settings
      #- xfce4-terminal
      - xfconf
      - xfdesktop
      - xfwm4
      - xfwm4-themes

/etc/X11/xorg.conf:
  file.managed:
    - source: salt://desktop/xorg.conf

/etc/lightdm/lightdm.conf:
  file.managed:
    - source: salt://desktop/lightdm.conf

/etc/asound.conf:
  file.managed:
    - source: salt://desktop/asound.conf

/etc/modprobe.d/sound.conf:
  file.managed:
    - source: salt://desktop/sound.conf

# TODO install murrine-themes

