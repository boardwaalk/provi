base:
  '*':
    - base
  'gammaburst.ersatsz.com':
    - xorg
    - desktop
    - gammaburst
  'inkan.ersatsz.com':
    - xorg
    - htpc
  'lysergic.ersatsz.com':
    - proxy
    - mail
    - www
    - www.git
    - www.news
    - www.ipy
  'bandit.ersatsz.com':
    - proxy
    - www
    - www.ipy
