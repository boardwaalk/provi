xorg-packages:
  pkg.installed:
    - pkgs:
      - xorg-server
      - lightdm
      - acpid
      - catalyst

acpid:
  service:
    - running
    - enable: True

atieventsd:
  service:
    - running
    - enable: True

temp-links-catalyst:
  service:
    - running
    - enable: True

lightdm:
  service:
    - running
    - enable: True

