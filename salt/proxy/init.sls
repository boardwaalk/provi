proxy-packages:
  pkg.installed:
    - pkgs:
      - polipo

/etc/polipo/config:
  file.managed:
    - source: salt://proxy/config

polipo:
  service:
    - running
    - enable: True

