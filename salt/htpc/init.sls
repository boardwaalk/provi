htpc-packages:
  pkg.installed:
    - pkgs:
      - xbmc

/etc/lightdm/lightdm.conf:
  file.managed:
    - source: salt://htpc/lightdm.conf

/etc/udev/rules.d/99-lirc.rules:
  file.managed:
    - source: salt://htpc/99-lirc.rules

