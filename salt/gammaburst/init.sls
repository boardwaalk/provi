gammaburst-packages:
  pkg.installed:
    - pkgs:
      - ntfs-3g
      - bftpd

/etc/systemd/system/dmsetup@.service:
  file.managed:
    - source: salt://gammaburst/dmsetup@.service

/etc/tenchi.dmtable:
  file.managed:
    - source: salt://gammaburst/tenchi.dmtable

dmsetup@tenchi:
  service:
    - running
    - enable: True

/etc/bftpd.conf:
  file.managed:
    - source: salt://gammaburst/bftpd.conf

bftpd:
  service:
    - running
    - enable: True

/mnt/tenchi:
  mount.mounted:
    - device: /dev/mapper/tenchi
    - fstype: ntfs
    - mkmnt: True
    - opts:
      - _netdev
