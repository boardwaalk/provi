mail-packages:
  pkg.installed:
    - pkgs:
      - postfix
      - dovecot
      - opendkim
      - perl-email-mime

/etc/dovecot/dovecot.conf:
  file.managed:
    - source: salt://mail/dovecot.conf

/etc/postfix/main.cf:
  file.managed:
    - source: salt://mail/main.cf

/etc/postfix/master.cf:
  file.managed:
    - source: salt://mail/master.cf

/etc/postfix/gpg.pl:
  file.managed:
    - source: salt://mail/gpg.pl
    - mode: 755

/etc/opendkim/opendkim.conf:
  file.managed:
    - source: salt://mail/opendkim.conf

/root/.forward:
  file.managed:
    - source: salt://mail/root.forward

/etc/sudoers.d/filter:
  file.managed:
    - source: salt://mail/sudoers.d.filter

# TODO keys!

dovecot:
  service:
    - running
    - enable: True

postfix:
  service:
    - running
    - enable: True

# Run this as root in /etc/opendkim:
# opendkim-genkey -r -s mail -d ersatsz.com
# Then update the zone file with the contents of /etc/opendkim/mail.txt
opendkim:
  service:
    - running
    - enable: True

filter:
  user.present:
    - createhome: False

