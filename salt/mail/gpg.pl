#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Long qw(GetOptionsFromArray);
use Email::MIME;
use Email::Address;
use Net::Domain qw(hostdomain);
use IPC::Open2;

sub sendmail {
  my ($mailStr) = @_;
  open my $fh, "|-", "/usr/bin/sendmail", "-G", "-i", @ARGV or die "failed to start sendmail: $!";
  print $fh $mailStr;
  close $fh;
}

sub contenttype_has_mimetype {
  my ($contenttype, $mimetype) = @_;
  my @parts = split /;/, $contenttype, 2;
  $parts[0] eq $mimetype;
}

sub clearsign {
  my ($user, $in) = @_;
  my ($gpg_out, $gpg_in);
  my $pid = open2($gpg_out, $gpg_in, "sudo", "-u", $user, "gpg", "--batch", "--clearsign");
  print $gpg_in $in;
  close $gpg_in;
  my $out = do { local $/; <$gpg_out>; };
  close $gpg_out;
  $out;
}

# parse arguments
my @rcpt = @ARGV;
my %options = ();
GetOptionsFromArray(\@rcpt, \%options, 'f=s') or die "failed to parse arguments";

# parse sender
my $sender = do {
  my @senders = Email::Address->parse($options{f});
  die "failed to parse sender" if scalar(@senders) != 1;
  $senders[0];
};

# read raw mail
my $mailStr = do { local $/; <STDIN> };

# passthrough if sender's hostname doesn't match the current domain
if($sender->host ne hostdomain()) {
  print STDERR "sender hostname does not match domainname\n";
  sendmail($mailStr);
  exit;
}

# parse mail
my $mail = Email::MIME->new($mailStr);

# sign plain text parts
$mail->walk_parts(sub {
  my ($part) = @_;
    if(contenttype_has_mimetype($part->content_type, "text/plain")) {
      $part->body_str_set(clearsign($sender->user, $part->body_str));
    }
  });

# send modified mail
sendmail($mail->as_string);

