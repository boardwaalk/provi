www-ipy-packages:
  pkg.installed:
    - pkgs:
      - ipython

/etc/systemd/system/ipython.service:
  file.managed:
    - source: salt://www/ipy/ipython.service

ipython:
  service:
    - running
    - enable: True

/etc/nginx/sites/ipy.conf:
  file.managed:
    - source: salt://www/ipy/nginx-ipy.conf

