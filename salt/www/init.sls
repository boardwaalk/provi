www-packages:
  pkg.installed:
    - pkgs:
      - nginx

/etc/nginx/nginx.conf:
  file.managed:
    - source: salt://www/nginx.conf

/etc/nginx/htpasswd:
  file.managed:
    - contents_pillar: htpasswd

/etc/nginx/sites:
  file.directory

nginx:
  service:
    - running
    - enable: True

