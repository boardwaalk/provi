root:
  user.present:
    - password: {{ pillar['root-password-hash'] }}

boardwalk:
  user.present:
    - password: {{ pillar['boardwalk-password-hash'] }}
    - shell: /bin/bash
    - fullname: Dan Skorupski
    - createhome: True
    - groups:
      - wheel
    - optional_groups:
      - autologin

/home/boardwalk:
  file.recurse:
    - source: salt://base/dotfiles
    - user: boardwalk
    - group: boardwalk
    - exclude_pat: .git

