base-packages:
  pkg.installed:
    - pkgs:
      - sudo
      - vim
      - tmux
      - openssh
      - ntp
      - ddclient
      # common tools needed for building software
      - fakeroot
      - autoconf
      - automake
      - libtool
      - pkg-config

base-rm-packages:
  pkg.removed:
    - pkgs:
      - heirloom-mailx
      - pcmciautils
      - jfsutils
      - reiserfsprogs
      - xfsprogs
      - vi

# use netctl enable to use
/etc/netctl/main:
  file.managed:
    - source: salt://base/netctl.{{ grains.host }}
    - template: jinja

/etc/sudoers:
  file.managed:
    - source: salt://base/sudoers

/etc/ssh/sshd_config:
  file.managed:
    - source: salt://base/sshd_config

/etc/hosts:
  file.managed:
    - source: http://someonewhocares.org/hosts/zero/hosts
    - source_hash: md5=a683b4d0890b987415280eca7adc3c8f

/etc/ddclient/ddclient.conf:
  file.managed:
    - source: salt://base/ddclient.conf
    - template: jinja

locale.gen:
  file.managed:
    - name: /etc/locale.gen
    - source: salt://base/locale.gen
  cmd.wait:
    - name: locale-gen
    - watch:
      - file: locale.gen

/etc/locale.conf:
  file.managed:
    - source: salt://base/locale.conf

/etc/localtime:
  file.symlink:
    - target: /usr/share/zoneinfo/US/Central

/etc/pacman.conf:
  file.managed:
    - source: salt://base/pacman.conf

sshd:
  service:
    - running
    - enable: True

ntpd:
  service:
    - running
    - enable: True

#ddclient:
#  service:
#    - running
#    - enable: True

include:
  - base.users

#pacman-keys:
#  cmd.script:
#    - source: salt://base/pacman-keys.sh

