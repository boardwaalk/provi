#!/bin/sh
set -e -x

check_env() {
  local name=$1
  local value
  eval value=\$$name
  if [ -z "$value" ]; then
    echo "$name must be defined."
    exit 1
  fi
}

# A friendly reminder: settings up a static IP
# ip addr flush dev $NETWORK_INTERFACE
# ip addr add $NETWORK_ADDRESS dev $NETWORK_INTERFACE
# ip route add $NETWORK_ADDRESS via $NETWORK_GATEWAY dev $NETWORK_INTERFACE

# If you want to enter the contents of KEY_FILE on the command line,
# make sure it does not containing a trailing newline! nano -L
check_env NETWORK_HOSTNAME # ex. example.com
check_env BLOCK_DEVICE # ex. /dev/sda
check_env KEY_FILE # ex. mypassword.txt

#
# Network setup
#

#
# Filesystem - Partitions
#
parted -s $BLOCK_DEVICE mklabel msdos
parted $BLOCK_DEVICE mkpart primary ext2 1M 64M
parted -- $BLOCK_DEVICE mkpart primary ext4 64M -1

#
# Filesystem - encryption
#
BOOT_PARTITION=${BLOCK_DEVICE}1
CRYPT_PARTITION=${BLOCK_DEVICE}2

cryptsetup luksFormat $CRYPT_PARTITION \
  --cipher aes-xts-plain64 --key-size 512 \
  --hash sha512 --iter-time 5000 --use-random --key-file $KEY_FILE

cryptsetup luksOpen $CRYPT_PARTITION lvmpool \
  --key-file $KEY_FILE

CRYPT_PARTITION_UUID=$(blkid $CRYPT_PARTITION -s UUID -o value)

#
# Filesystem - LVM
#
pvcreate /dev/mapper/lvmpool
vgcreate volgroup /dev/mapper/lvmpool
lvcreate -L 1G volgroup -n swap
lvcreate -l +100%FREE volgroup -n root

#
# Filesystem - formatting
#
mkswap /dev/volgroup/swap
mkfs.ext4 -m 1 /dev/volgroup/root
mkfs.ext2 $BOOT_PARTITION

#
# Filesystem - mounting
#
mount /dev/volgroup/root /mnt
mkdir /mnt/boot
mount $BOOT_PARTITION /mnt/boot
swapon /dev/volgroup/swap

pacstrap /mnt base
genfstab -U -p /mnt >> /mnt/etc/fstab
echo 'tmpfs /tmp tmpfs nodev,nosuid,size=2G 0 0' >> /mnt/etc/fstab

#
# Configuration and bootloader setup
#
arch-chroot /mnt <<EOF

set -e
echo $NETWORK_HOSTNAME > /etc/hostname
sed -i '/^HOOKS=/ s/block filesystems/block encrypt lvm2 filesystems/' /etc/mkinitcpio.conf
mkinitcpio -p linux
pacman-db-upgrade
pacman --noconfirm -S grub
grub-install --recheck $BLOCK_DEVICE
grub-mkconfig -o /boot/grub/grub.cfg # Makes noise, seems to be okay
sed -i -r "/^\s+linux\s/ s,\$, cryptdevice=/dev/disk/by-uuid/${CRYPT_PARTITION_UUID}:lvmpool," /boot/grub/grub.cfg

EOF

#
# Cleanup
#
umount /mnt/boot
umount /mnt
swapoff /dev/volgroup/swap
sleep 5 # Seems to be needed
vgchange -an volgroup
cryptsetup luksClose lvmpool
reboot

